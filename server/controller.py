# -*- coding: utf-8 -*-
# Copyright tracktoor. All rights reserved.
# 18/03/18
# @author: Victor Vicente de Carvalho

import logging
import uuid
import copy
from lxml import etree

from file_management import build_files_list, copy_file, file_length, FileDesc

logger = logging.getLogger('controller')


def boostrap_server(source_directory, server_directory, server_dictionary_file=None):
    tree, dom = create_dom_file()

    files = []
    build_files_list(source_directory, files)

    for file in files:
        dst_file = '{server_directory}/{filename}.srv'.format(
            server_directory=server_directory, filename=str(uuid.uuid4()))

        copy_file(file.path, dst_file)

        new_document = etree.SubElement(dom, 'document')
        etree.SubElement(new_document, 'document_name').text = file.name
        etree.SubElement(new_document, 'version').text = '1'
        etree.SubElement(new_document, 'path_to_file').text = dst_file
        etree.SubElement(new_document, 'length').text = str(
            file_length(dst_file))

    if server_dictionary_file:
        tree.write(server_dictionary_file, pretty_print=True)

    return tree, dom


def verify_updates(dom_tree, server_dictionary_file):
    dom = dom_tree.getroot()

    for document in dom.xpath('./document'):
        path = document.find('path_to_file').text
        length = file_length(path)
        length_node = document.find('length')
        document_length = int(length_node.text)

        if document_length != length:
            logger.info('Document {path} has different lengths. '
                        'Updating version.'.format(path=path))

            version = document.find('version')
            version.text = str(int(version.text) + 1)
            length_node.text = str(length)

    dom_tree.write(server_dictionary_file)


def get_client_xml_and_files(xml_bytes, server_xml_dom):
    if xml_bytes:
        xml = etree.fromstring(xml_bytes)
    else:
        xml = etree.Element('documents')

    files = []
    for document in server_xml_dom.xpath('./document'):
        document_name = document.find('document_name').text
        client_node = xml.xpath('./document/document_name[text()="{docname}"]/..'.format(docname=document_name))

        server_version = document.find('version').text
        if client_node:
            client_node = client_node[0]
            version = client_node.find('version').text

            if version == server_version:
                xml.remove(client_node)
                continue

            client_node.find('version').text = server_version
            client_node.find('length').text = document.find('length').text
        else:
            client_node = copy.deepcopy(document)
            client_node.remove(client_node.find('path_to_file'))
            xml.append(client_node)

        files.append(FileDesc(document_name, document.find('path_to_file').text))

    return xml, files


def create_dom_file():
    root = etree.Element('documents')
    tree = etree.ElementTree(root)
    return tree, root


def save_dom_file(tree, destination):
    tree.write(tree, destination)
