# -*- coding: utf-8 -*-
# Copyright tracktoor. All rights reserved.
# 18/03/18
# @author: Victor Vicente de Carvalho
import os
import logging

from lxml import etree
from argparse import ArgumentParser

from .api import app
from . import controller

logger = logging.getLogger('main')


def initialize(app, args):
    if not os.path.isfile(args.server_dictionary):
        raise ValueError(
            'The server dictionary file [{server_dictionary}] does not exist.'.format(server_dictionary=args))

    tree = etree.parse(args.server_dictionary)
    app.server_xml = tree.getroot()

    controller.verify_updates(tree, args.server_dictionary)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    parser = ArgumentParser('file-server')
    parser.add_argument('--server-folder',
                        help='The folder to set up the synchronization')
    parser.add_argument('--server-dictionary', required=True,
                        help='The dictionary containing the server versioning schema')

    parser.add_argument('--bootstrap-folder',
                        help='The folder from where to boostrat the server dictionary file')

    args = parser.parse_args()

    if args.bootstrap_folder:
        controller.boostrap_server(args.bootstrap_folder, args.server_folder,
                                   server_dictionary_file=args.server_dictionary)
        logger.info('Boostrapped the server dictionary file on {file}'
                    .format(file=args.server_dictionary))
    else:
        initialize(app, args)
        app.run()
