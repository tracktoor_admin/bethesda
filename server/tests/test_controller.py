# -*- coding: utf-8 -*-
# Copyright tracktoor. All rights reserved.
# 18/03/18
# @author: Victor Vicente de Carvalho
import os
import random
import shutil
import string
import tempfile
import unittest
from lxml import etree
from os import makedirs

from .. import controller


def random_string(size):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(size))


def add_random_file(fdir, filename, size=None):
    try:
        makedirs(fdir)
    except OSError:
        pass

    with open('{dir}/{filename}'.format(dir=fdir, filename=filename), 'w') as target:
        target.write(random_string(size or random.randint(0, 65535)))


class ControllerTests(unittest.TestCase):

    def setUp(self):
        self.src_dir = tempfile.mkdtemp()
        self.dst_dir = tempfile.mkdtemp()
        self.tmp_xml_file = tempfile.mktemp()

    def tearDown(self):
        shutil.rmtree(self.src_dir)
        shutil.rmtree(self.dst_dir)
        if os.path.isfile(self.tmp_xml_file):
            os.remove(self.tmp_xml_file)

    def test_bootstrap_server(self):
        add_random_file(self.src_dir, 'a.txt', size=10)

        _, dom = controller.boostrap_server(self.src_dir, self.dst_dir)

        assert dom.find('./document') is not None
        assert dom.xpath('./document/document_name[text()="a.txt"]') is not None

    def test_bootstrap_server_nested(self):
        add_random_file(self.src_dir + '/subfolder/', 'test.txt', size=10)

        _, dom = controller.boostrap_server(self.src_dir, self.dst_dir)

        assert dom.find('./document') is not None
        assert dom.xpath('./document/document_name[text()="subfolder/test.txt"]') is not None

    def test_verify_updates(self):
        add_random_file(self.src_dir, 'a.txt', size=25)
        tree, dom = controller.boostrap_server(self.src_dir, self.dst_dir)
        path = dom.xpath('./document/path_to_file/text()')[0]

        add_random_file(os.path.dirname(path), os.path.basename(path), size=42)

        controller.verify_updates(tree, self.tmp_xml_file)

        tree = etree.parse(self.tmp_xml_file)
        dom = tree.getroot()

        element = dom.find('./document')
        assert element.find('version').text == '2'
        assert element.find('length').text == '42'


class ControllerGetFilesTests(unittest.TestCase):
    def setUp(self):
        self.src_dir = tempfile.mkdtemp()
        self.dst_dir = tempfile.mkdtemp()

        add_random_file(self.src_dir, 'a.txt', size=25)
        add_random_file(self.src_dir, 'b.txt', size=33)
        self.files = [{'document_name': 'a.txt', 'version': '1', 'length': '25'},
                      {'document_name': 'b.txt', 'version': '1', 'length': '33'}]

        _, dom = controller.boostrap_server(self.src_dir, self.dst_dir)
        self.server_dom = dom

    def tearDown(self):
        shutil.rmtree(self.src_dir)
        shutil.rmtree(self.dst_dir)

    def test_get_client_xml_and_files_empty(self):
        xml, files = controller.get_client_xml_and_files(None, self.server_dom)

        assert len(xml.xpath('./document')) == 2
        assert len(files) == 2

    def _make_client_xml(self, *files):
        client_xml = etree.Element('documents')

        for dkt in self.files:
            if dkt['document_name'] in files:
                sub = etree.SubElement(client_xml, 'document')
                etree.SubElement(sub, 'document_name').text = dkt['document_name']
                etree.SubElement(sub, 'version').text = dkt['version']
                etree.SubElement(sub, 'length').text = dkt['length']

        return client_xml

    def _set_server_version(self, document_name, version):
        node = self.server_dom.xpath('./document/document_name[text()="{docname}"]'
                                     '/../version'.format(docname=document_name))[0]
        node.text = version

    def test_get_client_xml_and_files_nonempty(self):
        client_xml = self._make_client_xml('a.txt')

        xml, files = controller.get_client_xml_and_files(etree.tostring(client_xml), self.server_dom)

        assert len(xml.xpath('./document')) == 1
        assert len(files) == 1
        assert xml.xpath('./document/document_name')[0].text == 'b.txt'

    def test_get_client_xml_and_files_nonempty_version(self):
        client_xml = self._make_client_xml('a.txt', 'b.txt')
        self._set_server_version('a.txt', '2')

        xml, files = controller.get_client_xml_and_files(etree.tostring(client_xml), self.server_dom)

        assert len(xml.xpath('./document')) == 1
        assert len(files) == 1
        assert xml.xpath('./document/document_name')[0].text == 'a.txt'
