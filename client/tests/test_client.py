# -*- coding: utf-8 -*-
# Copyright tracktoor. All rights reserved.
# 18/03/18
# @author: Victor Vicente de Carvalho
import unittest
from lxml import etree

from .. import main


class ControllerTests(unittest.TestCase):
    def _make_client_xml(self, *files):
        client_xml = etree.Element('documents')

        for dkt in self.files:
            if dkt['document_name'] in files:
                sub = etree.SubElement(client_xml, 'document')
                etree.SubElement(sub, 'document_name').text = dkt['document_name']
                etree.SubElement(sub, 'version').text = dkt['version']
                etree.SubElement(sub, 'length').text = dkt['length']

        return client_xml

    def setUp(self):
        self.files = [{'document_name': 'a.txt', 'version': '1', 'length': '25'},
                      {'document_name': 'b.txt', 'version': '1', 'length': '33'}]

    def test_extract_filename(self):
        assert main.extract_filename('a="banana";c="blah";filename="test.txt"') == 'test.txt'

    def test_merge(self):
        dom_a = self._make_client_xml('a.txt')
        dom_b = self._make_client_xml('b.txt')

        main.merge_doms(dom_a, dom_b)

        assert len(dom_a.xpath('./document')) == 2

    def test_merge_update(self):
        dom_a = self._make_client_xml('a.txt', 'b.txt')
        dom_b = self._make_client_xml('b.txt')
        dom_b.find('./document/version').text = '2'

        main.merge_doms(dom_a, dom_b)

        node = dom_a.xpath('./document/document_name[text()="b.txt"]/..')[0]

        assert node.find('./version').text == '2'
